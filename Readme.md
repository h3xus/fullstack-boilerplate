This scaffold provides a structured environment for a **Java backend** and an **Angular frontend** project. It includes Docker configurations for building and running both parts of the application in containers, ensuring consistency across different development and production environments. 

- **Backend**: Built using Maven and runs on OpenJDK 11.
- **Frontend**: Built with Node.js and served via Nginx.

Additionally, the scaffold includes:
- **MariaDB** for database services.
- **Adminer** for database management.

All components are networked within a Docker-defined bridge network.

